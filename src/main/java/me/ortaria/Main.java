package me.ortaria;
import me.ortaria.metier.Alchimiste;
import me.ortaria.metier.Forgeron;
import me.ortaria.metier.Mineur;
import me.ortaria.metier.Pelleteur;
import me.ortaria.metier.commandes.alchimiste.CommandGivePotion;
import me.ortaria.metier.commandes.forgeron.CommandTestCraft;
import me.ortaria.metier.commandes.mineur.*;
import me.ortaria.metier.listeners.AlchimisteListener;
import me.ortaria.metier.listeners.ForgeronListener;
import me.ortaria.mob.boss.BossListener;
import me.ortaria.mob.boss.BossMinecart;
import me.ortaria.utils.Dice;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin{
    Mineur mineur;
    Forgeron forgeron;
    Pelleteur pelleteur;
    Dice dice;
    Alchimiste alchimiste;

    public Mineur getMineur() {
        return mineur;
    }
    public Forgeron getForgeron(){return forgeron;}
    public Alchimiste getAlchimiste(){return alchimiste;}
    public Pelleteur getPelleteur() {return pelleteur;}

    @Override
    public void onEnable() {
        this.dice = new Dice();
        this.mineur = new Mineur(dice);
        this.forgeron = new Forgeron();
        this.pelleteur = new Pelleteur(dice);
        this.alchimiste = new Alchimiste();

        getCommand("minerais").setExecutor(new CommandMinerais());
        getCommand("mineurXp").setExecutor(new CommandMineurXp());
        getCommand("setMineur").setExecutor(new CommandSetMineur());
        getCommand("showMineur").setExecutor(new CommandShowMineur());
        getCommand("tb").setExecutor(new CommandTb());
        getCommand("testCraft").setExecutor(new CommandTestCraft());
        getCommand("testBoss").setExecutor(new BossMinecart());
        getCommand("givePotion").setExecutor(new CommandGivePotion());

        getServer().getPluginManager().registerEvents(new PluginListeners(this), this);
        getServer().getPluginManager().registerEvents(new ForgeronListener(this), this);
        getServer().getPluginManager().registerEvents(new BossListener(dice), this);
        getServer().getPluginManager().registerEvents(new AlchimisteListener(this), this);

    }
}