package me.ortaria;

import me.ortaria.metier.ressource.mineur.PierrePrecieuse;
import me.ortaria.mob.boss.BossMinecart;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class PluginListeners implements Listener {
    private Main main;

    public PluginListeners(Main main){
        this.main = main;
    }

    @EventHandler
    public void BlockBreakEvent(BlockBreakEvent event){
        Player player = event.getPlayer();
        String block = event.getBlock().getTranslationKey();

        new BossMinecart();
        if(main.dice.tauxDrop(0.00005)){
            player.getInventory().addItem(new PierrePrecieuse(1));
            player.updateInventory();
        }
        if(main.getMineur().getMineurBlocs().contains(block)){
            event.setDropItems(false);
            event.setExpToDrop(0);
            main.getMineur().mineBloc(player, block);
        }
        if(main.getPelleteur().getPelleteurBlocs().contains(block)){
            main.getPelleteur().mineBlock(player);
        }




    }

    @EventHandler
    public void onInteract(PlayerInteractEvent event){
        Player player = event.getPlayer();
        Action action = event.getAction();
        ItemStack it = event.getItem();

        if(event.getClickedBlock() != null && (event.getClickedBlock().getType() == Material.ANVIL || event.getClickedBlock().getType() == Material.CHIPPED_ANVIL || event.getClickedBlock().getType() == Material.DAMAGED_ANVIL)){
            event.setCancelled(true);
            main.getForgeron().menu(player);
        }
        if(event.getClickedBlock() != null && event.getClickedBlock().getType() == Material.BREWING_STAND){
            event.setCancelled(true);
            main.getAlchimiste().menu(player);
        }
    }

}
