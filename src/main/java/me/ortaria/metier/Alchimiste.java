package me.ortaria.metier;

import me.ortaria.metier.ressource.alchimiste.PotionRegeneration;
import me.ortaria.metier.ressource.alchimiste.PotionSoin;
import me.ortaria.utils.GestionItem;
import me.ortaria.utils.GestionMenu;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Alchimiste {

    int level = 4;

    public void menu(Player player){
        Inventory inv = Bukkit.createInventory(null, 27, "==========Alchimiste==========");
        for(int i = 0; i <= 26; i ++){
            inv.setItem(i, new ItemStack(Material.WHITE_STAINED_GLASS_PANE, 1));
            if(i == 9)i=16;
        }
        inv.setItem(11, GestionItem.getItem(Material.BREWING_STAND, "§e[Alchimiste 1]"));
        player.openInventory(inv);
    }


    public void menuAlchi1(Player player){
        Inventory inv = Bukkit.createInventory(null, 45, "==========Alchimiste 1==========");
        GestionMenu.fillBorder45(inv, Material.WHITE_STAINED_GLASS_PANE);

        inv.setItem(21, PotionRegeneration.DisplayItem());

        inv.setItem(23, PotionSoin.DisplayItem());

        player.openInventory(inv);


    }


}
