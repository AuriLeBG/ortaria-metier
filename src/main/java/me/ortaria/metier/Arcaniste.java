package me.ortaria.metier;

import me.ortaria.Main;
import me.ortaria.metier.ressource.arcaniste.base.ArcaneDeLapprenti;
import me.ortaria.metier.ressource.arcaniste.base.ArcaneDuDébutant;
import me.ortaria.metier.ressource.arcaniste.tier1.*;
import me.ortaria.metier.ressource.arcaniste.tier2.*;
import me.ortaria.utils.Dice;
import me.ortaria.utils.GestionItem;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Arcaniste {
    Dice dice;
    public Arcaniste(Dice dice){
        this.dice = dice;
    }

    int level = 3;

    public void enchantement(Player player, ItemStack item){
        if(item instanceof ArcaneDuDébutant){
            enchantTier1(player);
        }
        else if(item instanceof ArcaneDeLapprenti){
            enchantTier2(player);
        }
    }

    private void enchantTier2(Player player) {
        int roll = dice.nombreDrop(1, 5);
        GestionItem.retirerItemSlot(player, 1);
        switch (roll){
            case 1:
                player.getInventory().addItem(new Tier2Force(dice));
            case 2:
                player.getInventory().addItem(new Tier2FaveurAstrale(dice));
            case 3:
                player.getInventory().addItem(new Tier2PV(dice));
            case 4:
                player.getInventory().addItem(new Tier2RechargeAstrale(dice));
            case 5:
                player.getInventory().addItem(new Tier2Vitesse(dice));
        }

    }


    public void enchantTier1(Player player){
        int roll = dice.nombreDrop(1, 5);
        GestionItem.retirerItemSlot(player, 1);
        switch (roll){
            case 1:
                player.getInventory().addItem(new Tier1Force(dice));
            case 2:
                player.getInventory().addItem(new Tier1FaveurAstrale(dice));
            case 3:
                player.getInventory().addItem(new Tier1PV(dice));
            case 4:
                player.getInventory().addItem(new Tier1RechargeAstrale(dice));
            case 5:
                player.getInventory().addItem(new Tier1Vitesse(dice));
        }
    }
}
