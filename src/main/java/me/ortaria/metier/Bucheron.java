package me.ortaria.metier;

import me.ortaria.metier.ressource.bucheron.*;
import me.ortaria.utils.Dice;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;

public class Bucheron {
    int level = 7;
    Dice dice;
    List<String> blocksBucheron;

    public Bucheron(Dice dice){
        this.dice = dice;
        this.blocksBucheron = Arrays.asList("block.minecraft.oak_log", "block.minecraft.spruce_log", "block.minecraft.birch_log", "block.minecraft.jungle_log", "block.minecraft.acacia_log", "block.minecraft.dark_oak_log", "block.minecraft.mangrove_log", "block.minecraft.cherry_log");
    }

    public void mineBuche(Player player){
        double[] dropBrancheRobuste = {0.1, 0.2, 0.3, 0.4, 0.5, 0.6};
        double[] dropBucheRobuste = {0, 0.1, 0.2, 0.3, 0.4, 0.5};
        double[] dropBrancheRare = {0, 0, 0.1, 0.2, 0.3, 0.4};
        double[] dropBucheRare = {0, 0, 0, 0.1, 0.2, 0.3};
        double[] dropBrancheLegendaire = {0, 0, 0, 0, 0.1, 0.2};
        double[] dropBucheLegendaire = {0, 0, 0, 0, 0, 0.1};

        if(dice.tauxDrop(dropBrancheRobuste[level])){
            player.getInventory().addItem(new BrancheRobuste(1));
            player.updateInventory();
        }

        if(dice.tauxDrop(dropBucheRobuste[level])){
            player.getInventory().addItem(new BucheRobuste(1));
            player.updateInventory();
        }

        if(dice.tauxDrop(dropBrancheRare[level])){
            player.getInventory().addItem(new BrancheRare(1));
            player.updateInventory();
        }

        if(dice.tauxDrop(dropBucheRare[level])){
            player.getInventory().addItem(new BucheRobuste(1));
            player.updateInventory();
        }

        if(dice.tauxDrop(dropBrancheLegendaire[level])){
            player.getInventory().addItem(new BranchageLégendaire(1));
            player.updateInventory();
        }

        if(dice.tauxDrop(dropBucheLegendaire[level])){
            player.getInventory().addItem(new BucheLégendaire(1));
            player.updateInventory();
        }

        if(dice.tauxDrop(0.000005)){
            player.getInventory().addItem(new BoisDesDieux(1));
            player.updateInventory();
        }
    }
}
