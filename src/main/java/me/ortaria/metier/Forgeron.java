package me.ortaria.metier;

import me.ortaria.metier.ressource.forgeron.tier1.*;
import me.ortaria.metier.ressource.forgeron.tier2.*;
import me.ortaria.metier.ressource.forgeron.tier3.*;
import me.ortaria.metier.ressource.forgeron.tier4.*;
import me.ortaria.utils.GestionItem;
import me.ortaria.utils.GestionMenu;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class Forgeron{
    int level = 4;

    public void menu(Player player){
        Inventory inv = Bukkit.createInventory(null, 27, "==========Forgeron==========");
        for(int i = 0; i <= 26; i ++){
            inv.setItem(i, new ItemStack(Material.WHITE_STAINED_GLASS_PANE, 1));
            if(i == 9)i=16;
        }
        inv.setItem(11, GestionItem.getItem(Material.ANVIL, "§e[Forgeron 1]"));

        if(level >= 2){
            inv.setItem(12, GestionItem.getItem(Material.ANVIL, "§e[Forgeron 2]"));
        }
        if(level >= 3){
            inv.setItem(14, GestionItem.getItem(Material.ANVIL, "§e[Forgeron 3]"));
        }
        if(level >= 4) {
            inv.setItem(15, GestionItem.getItem(Material.ANVIL, "§e[Forgeron 4]"));
        }

        player.openInventory(inv);

    }

    public void menuTier1(Player player){
        Inventory inv = Bukkit.createInventory(null, 54, "==========Forgeron 1==========");
        GestionMenu.fillBorder54(inv, Material.WHITE_STAINED_GLASS_PANE);
        inv.setItem(11, CasqueRenforcé.displayItem());
        inv.setItem(20, PlastronRenforcé.displayItem());
        inv.setItem(29, JambièresRenforcées.displayItem());
        inv.setItem(38, BottesRenforcées.displayItem());
        inv.setItem(24, EpéeRenforcée.displayItem());
        inv.setItem(33, PiocheRenforcée.displayItem());



        player.openInventory(inv);
    }


    public void menuTier2(Player player) {
        Inventory inv = Bukkit.createInventory(null, 54, "==========Forgeron 2==========");
        GestionMenu.fillBorder54(inv, Material.WHITE_STAINED_GLASS_PANE);
        inv.setItem(11, SuperCasqueRenforcé.displayItem());
        inv.setItem(20, SuperPlastronRenforcé.displayItem());
        inv.setItem(29, SuperJambièresRenforcées.displayItem());
        inv.setItem(38, SuperBottesRenforcées.displayItem());
        inv.setItem(24, SuperEpéeRenforcée.displayItem());
        inv.setItem(33, SuperPiocheRenforcée.displayItem());

        player.openInventory(inv);
    }

    public void menuTier3(Player player) {
        Inventory inv = Bukkit.createInventory(null, 54, "==========Forgeron 3==========");
        GestionMenu.fillBorder54(inv, Material.WHITE_STAINED_GLASS_PANE);
        inv.setItem(11, HeroicCasqueRenforcé.displayItem());
        inv.setItem(20, HeroicPlastronRenforcé.displayItem());
        inv.setItem(29, HeroicJambièresRenforcées.displayItem());
        inv.setItem(38, HeroicBottesRenforcées.displayItem());
        inv.setItem(24, HeroicEpéeRenforcée.displayItem());
        inv.setItem(33, HeroicPiocheRenforcée.displayItem());
        player.openInventory(inv);

    }

    public void menuTier4(Player player) {
        Inventory inv = Bukkit.createInventory(null, 54, "==========Forgeron 4==========");
        GestionMenu.fillBorder54(inv, Material.WHITE_STAINED_GLASS_PANE);
        inv.setItem(11, LegendaryCasqueRenforcé.displayItem());
        inv.setItem(20, LegendaryPlastronRenforcé.displayItem());
        inv.setItem(29, LegendaryJambièresRenforcées.displayItem());
        inv.setItem(38, LegendaryBottesRenforcées.displayItem());
        inv.setItem(24, LegendaryEpéeRenforcée.displayItem());
        inv.setItem(33, LegendaryPiocheRenforcée.displayItem());

        player.openInventory(inv);


    }
}
