package me.ortaria.metier;

import me.ortaria.metier.ressource.mineur.*;
import me.ortaria.utils.Dice;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class Mineur {
    ArrayList<String> mineurBlocs = new ArrayList<>();
    Dice dice;
    
    int xpPlayer;
    int levelPlayer;
    int[] xpLevelUp;

    public ArrayList<String> getMineurBlocs() {
        return mineurBlocs;
    }

    public Mineur(Dice dice){
        mineurBlocs.add("block.minecraft.iron_ore");
        mineurBlocs.add("block.minecraft.deepslate_iron_ore");
        mineurBlocs.add("block.minecraft.copper_ore");
        mineurBlocs.add("block.minecraft.deepslate_copper_ore");
        mineurBlocs.add("block.minecraft.gold_ore");
        mineurBlocs.add("block.minecraft.deepslate_gold_ore");
        mineurBlocs.add("block.minecraft.diamond_ore");
        mineurBlocs.add("block.minecraft.deepslate_diamond_ore");
        mineurBlocs.add("block.minecraft.redstone_ore");
        mineurBlocs.add("block.minecraft.deepslate_redstone_ore");
        mineurBlocs.add("block.minecraft.emerald_ore");
        mineurBlocs.add("block.minecraft.deepslate_emerald_ore");
        mineurBlocs.add("block.minecraft.lapis_ore");
        mineurBlocs.add("block.minecraft.deepslate_lapis_ore");
        mineurBlocs.add("block.minecraft.obsidian");
        this.dice = dice;
        this.xpPlayer = 0;
        this.levelPlayer = 7;
        this.xpLevelUp = new int[]{1700, 2700, 5900, 16000, 35000, 65000, 85000, 10000, 120000};

    }


    public void mineBloc(Player player, String block) {
        player.sendMessage(player.getName());
        if(block.equals("block.minecraft.iron_ore") || block.equals("block.minecraft.deepslate_iron_ore")){
            minerIron(player);
        }
        else if(block.equals("block.minecraft.copper_ore") || block.equals("block.minecraft.deepslate_copper_ore")){
            minerCopper(player);
        }
        else if(block.equals("block.minecraft.gold_ore") || block.equals("block.minecraft.deepslate_gold_ore")){
            minerGold(player);
        }
        else if(block.equals("block.minecraft.diamond_ore") || block.equals("block.minecraft.deepslate_diamond_ore")){
            minerDiamond(player);
        }
        else if(block.equals("block.minecraft.redstone_ore") || block.equals("block.minecraft.deepslate_redstone_ore")){
            minerRedstone(player);
        }
        else if(block.equals("block.minecraft.emerald_ore") || block.equals("block.minecraft.deepslate_emerald_ore")){
            minerEmerald(player);
        }
        else if(block.equals("block.minecraft.lapis_ore") || block.equals("block.minecraft.deepslate_lapis_ore")){
            player.sendMessage(block);
            minerLapis(player);
        }
        else if(block.equals("block.minecraft.obsidian")){
            player.sendMessage("§a[+50xp]");
            xpPlayer += 50;
        }
        levelUp();
    }

    private void levelUp(){
        if(xpPlayer > xpLevelUp[levelPlayer - 1]){
            xpPlayer -= xpLevelUp[levelPlayer - 1];
            levelPlayer += 1;
        }
    }

    private void minerLapis(Player player) {
        int[] minRollDrop = {1, 1, 1, 1, 2, 2, 2, 2, 2, 3};
        int[] maxRollDrop = {5, 5, 6, 6, 7, 8, 9, 10, 10 , 10};
        double[] dropRare = {0, 0, 5, 5.5, 6, 6.5, 7, 7.5, 7.5, 8};
        player.getInventory().addItem(new ItemStack(Material.LAPIS_LAZULI, dice.nombreDrop(minRollDrop[levelPlayer - 1], maxRollDrop[levelPlayer - 1])));
        if(dice.tauxDrop(dropRare[levelPlayer - 1])){
            player.getInventory().addItem(new Saphir(1));
        }
        player.updateInventory();
        this.xpPlayer += 25;
        player.sendMessage("§a[+25xp]");
    }

    private void minerEmerald(Player player) {
        int[] minRollDrop = {1, 1, 1, 1, 1, 1, 1, 2, 2, 3};
        int[] maxRollDrop = {2, 2, 2, 2, 2, 3, 5, 5, 6, 6};
        double[] dropRare = {0, 0, 0, 0, 0, 2, 3, 4, 4, 5};
        player.getInventory().addItem(new ItemStack(Material.EMERALD, dice.nombreDrop(minRollDrop[levelPlayer - 1], maxRollDrop[levelPlayer - 1])));
        if(dice.tauxDrop(dropRare[levelPlayer - 1])){
            player.getInventory().addItem(new Adamantium(1));
        }
        player.updateInventory();
        this.xpPlayer += 100;
        player.sendMessage("§a[+100xp]");
    }

    private void minerRedstone(Player player) {
        int[] minRollDrop = {1, 1, 2, 2, 2, 2, 2, 3, 3, 3};
        int[] maxRollDrop = {6, 6, 6, 7, 7, 8, 8, 10, 10, 12};
        double[] dropRare = {0, 0, 5, 5.5, 6, 6.5, 7, 7.5, 7.5, 8.5};
        player.getInventory().addItem(new ItemStack(Material.REDSTONE, dice.nombreDrop(minRollDrop[levelPlayer - 1], maxRollDrop[levelPlayer - 1])));
        if(dice.tauxDrop(dropRare[levelPlayer - 1])){
            player.getInventory().addItem(new Rubis(1));
        }
        player.updateInventory();
        this.xpPlayer += 25;
        player.sendMessage("§a[+25xp]");
    }

    private void minerDiamond(Player player) {
        int[] minRollDrop = {1, 1, 1, 1, 1, 1, 2, 2, 2, 3};
        int[] maxRollDrop = {2, 2, 2, 2, 4, 5, 4, 5, 6, 6};
        double[] dropRare = {0, 0, 0, 0, 2, 2.5, 3, 3.5, 4, 5};
        player.getInventory().addItem(new ItemStack(Material.DIAMOND, dice.nombreDrop(minRollDrop[levelPlayer - 1], maxRollDrop[levelPlayer - 1])));
        if(dice.tauxDrop(dropRare[levelPlayer - 1])){
            player.getInventory().addItem(new Mithril(1));
        }
        player.updateInventory();
        this.xpPlayer += 70;
        player.sendMessage("§a[+70xp]");
    }

    private void minerGold(Player player) {
        int[] minRollDrop = {1, 1, 1, 1, 1, 1, 1, 2, 2, 2};
        int[] maxRollDrop = {2, 2, 2, 2, 3, 3, 3, 4, 4, 4};
        double[] dropRare = {0, 0, 0, 0, 2, 2.5, 3, 3.5, 4, 5};
        player.getInventory().addItem(new ItemStack(Material.GOLD_INGOT, dice.nombreDrop(minRollDrop[levelPlayer - 1], maxRollDrop[levelPlayer - 1])));
        player.updateInventory();
        this.xpPlayer += 40;
        player.sendMessage("§a[+40xp]");
    }

    private void minerCopper(Player player) {
        int[] minRollDrop = {1, 1 ,1, 1, 1, 1, 1, 1, 2, 2};
        int[] maxRollDrop = {1, 2, 3, 3, 4, 4, 5, 5, 5, 5};
        double[] dropRare = {0, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9};
        player.getInventory().addItem(new ItemStack(Material.COPPER_INGOT, dice.nombreDrop(minRollDrop[levelPlayer - 1], maxRollDrop[levelPlayer - 1])));
        if(dice.tauxDrop(dropRare[levelPlayer - 1])){
            player.getInventory().addItem(new Cuivre(1));
        }
        player.updateInventory();
        this.xpPlayer += 15;
        player.sendMessage("§a[+15xp]");
    }

    private void minerIron(Player player) {
        int[] minRollDrop = {1, 1, 1, 1, 1, 1, 2, 2, 2, 2};
        int[] maxRollDrop = {2, 2, 3, 3, 4, 4, 4, 4, 5, 5};
        double[] dropRare = {10, 10, 10.5, 10.5, 11, 11, 11.5, 11.5, 12, 12};
        player.getInventory().addItem(new ItemStack(Material.IRON_INGOT, dice.nombreDrop(minRollDrop[levelPlayer - 1], maxRollDrop[levelPlayer - 1])));
        if(dice.tauxDrop(dropRare[levelPlayer - 1])){
            player.getInventory().addItem(new Etain(1));
        }
        player.updateInventory();
        this.xpPlayer += 25;
        player.sendMessage("§a[+25xp]");
    }
}
