package me.ortaria.metier;

import me.ortaria.metier.ressource.pelleteur.*;
import me.ortaria.utils.Dice;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Pelleteur extends ItemStack {
    Dice dice;
    int level = 5;
    List<String> pelleteurBlocs;

    public List getPelleteurBlocs(){return pelleteurBlocs;}
    public Pelleteur(Dice dice){
        this.dice = dice;
        this.pelleteurBlocs = Arrays.asList("block.minecraft.grass_block", "block.minecraft.gravel", "block.minecraft.dirt", "block.minecraft.sand");
    }

    public void mineBlock(Player player){
        if(level >= 1){
            if(dice.tauxDrop(1)){
                player.getInventory().addItem(new FossileClair());
            }
        }
        if(level >= 2){
            if(dice.tauxDrop(1)){
                player.getInventory().addItem(new FossileEmbourbé());
            }
        }
        if(level >= 3){
            if(dice.tauxDrop(1)){
                player.getInventory().addItem(new FossileAcuminé());
            }
        }
        if(level >= 4){
            if(dice.tauxDrop(1)){
                player.getInventory().addItem(new FossilePétrifié());
            }
        }
        if(level >= 5){
            if(dice.tauxDrop(1)){
                player.getInventory().addItem(new FossilePlante());
            }
        }
    }
}
