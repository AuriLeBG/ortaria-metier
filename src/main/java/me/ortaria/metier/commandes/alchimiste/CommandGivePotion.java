package me.ortaria.metier.commandes.alchimiste;

import me.ortaria.metier.ressource.alchimiste.PotionRegeneration;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.PotionMeta;

public class CommandGivePotion implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof Player){
            Player player = (Player) commandSender;
            if(strings[0].equalsIgnoreCase("regeneration")){

                player.getInventory().addItem(new PotionRegeneration());
            }
        }
        return false;
    }
}
