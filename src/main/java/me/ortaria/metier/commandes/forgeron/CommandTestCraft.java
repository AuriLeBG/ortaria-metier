package me.ortaria.metier.commandes.forgeron;

import me.ortaria.metier.ressource.forgeron.tier1.CasqueRenforcé;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandTestCraft implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(commandSender instanceof Player){
            CasqueRenforcé.craft((Player) commandSender);
        }
        return false;
    }
}
