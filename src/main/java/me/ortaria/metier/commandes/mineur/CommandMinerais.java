package me.ortaria.metier.commandes.mineur;

import me.ortaria.metier.ressource.mineur.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CommandMinerais implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(strings.length == 0){
            commandSender.sendMessage("Veuillez préciser le minerais");
            return false;
        }
        if(commandSender instanceof Player){
            Player player = (Player) commandSender;
            player.sendMessage(strings[0]);
            if(strings[0].equalsIgnoreCase("etains")){
                player.getInventory().addItem(new ItemStack(new Etain(1)));
                player.updateInventory();
            }
            if(strings[0].equalsIgnoreCase("Adamantium")){
                player.getInventory().addItem(new ItemStack(new Adamantium(1)));
                player.updateInventory();
            }
            if(strings[0].equalsIgnoreCase("Cuivre")){
                player.getInventory().addItem(new ItemStack(new Cuivre(1)));
                player.updateInventory();
            }
            if(strings[0].equalsIgnoreCase("Mithril")){
                player.getInventory().addItem(new ItemStack(new Mithril(1)));
                player.updateInventory();
            }
            if(strings[0].equalsIgnoreCase("Ruby")){
                player.getInventory().addItem(new ItemStack(new Rubis(1)));
                player.updateInventory();
            }
            if(strings[0].equalsIgnoreCase("Saphir")){
                player.getInventory().addItem(new ItemStack(new Saphir(1)));
                player.updateInventory();
            }
        }
        return false;
    }
}
