package me.ortaria.metier.listeners;

import me.ortaria.Main;
import me.ortaria.metier.ressource.alchimiste.PotionRegeneration;
import me.ortaria.metier.ressource.alchimiste.PotionSoin;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BrewingStartEvent;
import org.bukkit.event.inventory.BrewingStandFuelEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class AlchimisteListener implements Listener {
    Main main;
    public AlchimisteListener(Main main) {
        this.main = main;
    }

    @EventHandler
    public void onBrewing(BrewingStandFuelEvent event){
        event.setCancelled(true);
    }

    @EventHandler
    public void onInteract(InventoryClickEvent event){
        Inventory inv = event.getInventory();
        Player player = (Player) event.getWhoClicked();
        ItemStack current = event.getCurrentItem();
        if(current == null){
            return;
        }
        if(event.getView().getTitle().equalsIgnoreCase("==========Alchimiste==========")) {
            if (current.getType() == Material.WHITE_STAINED_GLASS_PANE) {
                event.setCancelled(true);
                return;
            }
            if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§e[Alchimiste 1]")){
                main.getAlchimiste().menuAlchi1(player);
            }
        }
        else if(event.getView().getTitle().equalsIgnoreCase("==========Alchimiste 1==========")){
            if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§dPotion de régénération")){
                PotionRegeneration.craft(player);
                event.setCancelled(true);
                return;
            }
            if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§dPotion de soin")){
                PotionSoin.craft(player);
                event.setCancelled(true);
                return;
            }
        }
    }
}
