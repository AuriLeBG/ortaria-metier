package me.ortaria.metier.listeners;

import me.ortaria.Main;
import me.ortaria.metier.ressource.forgeron.tier1.*;
import me.ortaria.metier.ressource.forgeron.tier2.*;
import me.ortaria.metier.ressource.forgeron.tier3.*;
import me.ortaria.metier.ressource.forgeron.tier4.*;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ForgeronListener implements Listener {
    Main main;
    public ForgeronListener(Main main){this.main = main;}

    @EventHandler void onClick(InventoryClickEvent event){
        Inventory inv = event.getInventory();
        Player player = (Player) event.getWhoClicked();
        ItemStack current = event.getCurrentItem();



        if(current == null){
            return;
        }

        if(event.getView().getTitle().equalsIgnoreCase("==========Forgeron==========")){
            if(current.getType() == Material.WHITE_STAINED_GLASS_PANE){
                event.setCancelled(true);
                return;
            }
            player.sendMessage(current.getItemMeta().getDisplayName());
            if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§e[Forgeron 1]")){
                main.getForgeron().menuTier1(player);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§e[Forgeron 2]")){
                main.getForgeron().menuTier2(player);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§e[Forgeron 3]")){
                main.getForgeron().menuTier3(player);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§e[Forgeron 4]")){
                main.getForgeron().menuTier4(player);
                return;
            }
        }


        if(event.getView().getTitle().equalsIgnoreCase("==========Forgeron 1==========")){
            if(current.getType() == Material.WHITE_STAINED_GLASS_PANE){
                event.setCancelled(true);
                return;
            }
            if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Diamond boots renforcés]")){
                BottesRenforcées.craft(player);
                event.setCancelled(true);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Diamond helmet renforcé]")){
                CasqueRenforcé.craft(player);
                event.setCancelled(true);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Diamond sword renforcé]")){
                EpéeRenforcée.craft(player);
                event.setCancelled(true);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Diamond leggings renforcés]")){
                JambièresRenforcées.craft(player);
                event.setCancelled(true);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Diamond pickaxe renforcé]")){
                PiocheRenforcée.craft(player);
                event.setCancelled(true);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Diamond chestplate renforcé]")){
                PlastronRenforcé.craft(player);
                event.setCancelled(true);
                return;
            }

        }
        else if(event.getView().getTitle().equalsIgnoreCase("==========Forgeron 2==========")){
            if(current.getType() == Material.WHITE_STAINED_GLASS_PANE){
                event.setCancelled(true);
                return;
            }

            if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Super Diamond boots renforcés]")){
                SuperBottesRenforcées.craft(player);
                event.setCancelled(true);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Super Diamond helmet renforcé]")){
                SuperCasqueRenforcé.craft(player);
                event.setCancelled(true);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Super Diamond sword renforcé]")){
                SuperEpéeRenforcée.craft(player);
                event.setCancelled(true);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Super Diamond leggings renforcés]")){
                SuperJambièresRenforcées.craft(player);
                event.setCancelled(true);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Super Diamond pickaxe renforcé]")){
                SuperPiocheRenforcée.craft(player);
                event.setCancelled(true);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Super Diamond chestplate renforcé]")){
               SuperPlastronRenforcé.craft(player);
                event.setCancelled(true);
                return;
            }
        }
        else if(event.getView().getTitle().equalsIgnoreCase("==========Forgeron 3==========")){
            if(current.getType() == Material.WHITE_STAINED_GLASS_PANE){
                event.setCancelled(true);
                return;
            }

            if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Heroic Diamond boots renforcés]")){
                HeroicBottesRenforcées.craft(player);
                event.setCancelled(true);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Heroic Diamond helmet renforcé]")){
                HeroicCasqueRenforcé.craft(player);
                event.setCancelled(true);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Heroic Diamond sword renforcé]")){
                HeroicEpéeRenforcée.craft(player);
                event.setCancelled(true);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Heroic Diamond leggings renforcés]")){
                HeroicJambièresRenforcées.craft(player);
                event.setCancelled(true);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Heroic Diamond pickaxe renforcé]")){
                HeroicPiocheRenforcée.craft(player);
                event.setCancelled(true);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Heroic Diamond chestplate renforcé]")){
                HeroicPlastronRenforcé.craft(player);
                event.setCancelled(true);
                return;
            }
        }
        else if(event.getView().getTitle().equalsIgnoreCase("==========Forgeron 4==========")){
            if(current.getType() == Material.WHITE_STAINED_GLASS_PANE){
                event.setCancelled(true);
                return;
            }

            if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Legendary Diamond boots renforcés]")){
                LegendaryBottesRenforcées.craft(player);
                event.setCancelled(true);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Legendary Diamond helmet renforcé]")){
                LegendaryCasqueRenforcé.craft(player);
                event.setCancelled(true);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Legendary Diamond sword renforcé]")){
                LegendaryEpéeRenforcée.craft(player);
                event.setCancelled(true);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Legendary Diamond leggings renforcés]")){
                LegendaryJambièresRenforcées.craft(player);
                event.setCancelled(true);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Legendary Diamond pickaxe renforcé]")){
                LegendaryPiocheRenforcée.craft(player);
                event.setCancelled(true);
                return;
            }
            else if(current.getItemMeta().getDisplayName().equalsIgnoreCase("§3[Legendary Diamond chestplate renforcé]")){
                LegendaryPlastronRenforcé.craft(player);
                event.setCancelled(true);
                return;
            }
        }


    }
}
