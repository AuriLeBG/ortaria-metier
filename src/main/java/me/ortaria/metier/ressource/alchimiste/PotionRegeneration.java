package me.ortaria.metier.ressource.alchimiste;

import me.ortaria.utils.GestionItem;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PotionRegeneration extends ItemStack {

    public PotionRegeneration(){
        super(Material.POTION, 1);
        PotionMeta meta = (PotionMeta) this.getItemMeta();
        meta.addCustomEffect(new PotionEffect(PotionEffectType.REGENERATION, 45 * 20, 0), true);
        meta.setColor(Color.fromARGB(2, 120, 53, 100)); //Couleur pas exacte mais j'ai fais au mieux
        meta.setDisplayName("§dPotion de régénération");
        this.setItemMeta(meta);
    }

    static public void craft(Player player){
        if(player.getInventory().contains(Material.BLAZE_ROD, 1)  && player.getInventory().contains(Material.GHAST_TEAR, 1)){
            for(int i = 0; i < player.getInventory().getSize(); i++){
                if(player.getInventory().getItem(i) != null){
                    if(player.getInventory().getItem(i).getTranslationKey().equalsIgnoreCase("item.minecraft.potion.effect.water")){
                        GestionItem.retirerItem(player, new ItemStack(Material.BLAZE_ROD, 1), 1);
                        GestionItem.retirerItem(player, new ItemStack(Material.GHAST_TEAR, 1), 1);
                        GestionItem.retirerWaterBottle(player, 1);
                        player.getInventory().addItem(new PotionRegeneration());
                        player.updateInventory();
                        return;
                    }
                }
            }
        }
        player.sendMessage("Vous n'avez pas les ressources nécessaires");
    }

    static public ItemStack  DisplayItem(){
        ItemStack item = new PotionRegeneration();
        GestionItem.modifyLore(item, "§e[§aWater bottle§e]");
        GestionItem.modifyLore(item, "§e[§aBlaze Rod§e]");
        GestionItem.modifyLore(item, "§e[§aGhast tear§e]");
        return item;

    }

}
