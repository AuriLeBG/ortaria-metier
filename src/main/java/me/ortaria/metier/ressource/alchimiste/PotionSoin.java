package me.ortaria.metier.ressource.alchimiste;

import me.ortaria.utils.GestionItem;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PotionSoin extends ItemStack {

    public PotionSoin(){
        super(Material.POTION, 1);
        PotionMeta meta = (PotionMeta) this.getItemMeta();
        meta.addCustomEffect(new PotionEffect(PotionEffectType.HEAL, 0, 0), true);
        meta.setColor(Color.RED); //Couleur pas exacte mais j'ai fais au mieux
        meta.setDisplayName("§dPotion de soin");
        this.setItemMeta(meta);
    }

    static public void craft(Player player){
        if(player.getInventory().contains(Material.BLAZE_ROD, 1)  && player.getInventory().contains(Material.GLISTERING_MELON_SLICE, 1)){
            for(int i = 0; i < player.getInventory().getSize(); i++){
                if(player.getInventory().getItem(i) != null){
                    if(player.getInventory().getItem(i).getTranslationKey().equalsIgnoreCase("item.minecraft.potion.effect.water")){
                        GestionItem.retirerItem(player, new ItemStack(Material.BLAZE_ROD, 1), 1);
                        GestionItem.retirerItem(player, new ItemStack(Material.GLISTERING_MELON_SLICE, 1), 1);
                        GestionItem.retirerWaterBottle(player, 1);
                        player.getInventory().addItem(new PotionSoin());
                        player.updateInventory();
                        return;
                    }
                }
            }
        }
        player.sendMessage("Vous n'avez pas les ressources nécessaires");
    }

    static public ItemStack  DisplayItem(){
        ItemStack item = new PotionSoin();
        GestionItem.modifyLore(item, "§e[§aWater bottle§e]");
        GestionItem.modifyLore(item, "§e[§aBlaze Rod§e]");
        GestionItem.modifyLore(item, "§e[§aGlistering melon§e]");
        return item;

    }
}
