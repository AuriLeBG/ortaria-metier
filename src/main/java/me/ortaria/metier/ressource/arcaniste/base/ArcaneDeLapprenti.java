package me.ortaria.metier.ressource.arcaniste.base;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ArcaneDeLapprenti extends ItemStack {
    public ArcaneDeLapprenti(){
        super(Material.BOOK, 1);
        ItemMeta meta = this.getItemMeta();
        meta.setDisplayName("§b[Arcane de l'apprenti]");
        this.setItemMeta(meta);
    }
}
