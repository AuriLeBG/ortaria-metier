package me.ortaria.metier.ressource.arcaniste.base;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class ArcaneDuDébutant extends ItemStack {
    public ArcaneDuDébutant(){
        super(Material.BOOK, 1);
        ItemMeta meta = this.getItemMeta();
        meta.setDisplayName("§b[Arcane du débutant]");
        this.setItemMeta(meta);
    }
}
