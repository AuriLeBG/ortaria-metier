package me.ortaria.metier.ressource.arcaniste.tier2;

import me.ortaria.utils.Dice;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class Tier2Force extends ItemStack {

    Dice dice;

    public Tier2Force(Dice dice){
        super(Material.BOOK, 1);
        this.dice = dice;
        ItemMeta meta = this.getItemMeta();
        int roll = dice.nombreDrop(5, 10);
        meta.setDisplayName("§l§c[FORCE §l§c+"+ roll + "§l§c]");
        meta.setLore(Arrays.asList("§6§l[RANG ✪✪]", "Force", String.valueOf(roll), "§0Enchanting"));
    }
}
