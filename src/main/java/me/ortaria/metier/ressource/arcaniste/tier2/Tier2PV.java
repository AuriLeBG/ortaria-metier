package me.ortaria.metier.ressource.arcaniste.tier2;

import me.ortaria.utils.Dice;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class Tier2PV extends ItemStack {

    Dice dice;

    public Tier2PV(Dice dice){
        super(Material.BOOK, 1);
        this.dice = dice;
        ItemMeta meta = this.getItemMeta();
        int roll = dice.nombreDrop(5, 10);
        meta.setDisplayName("§l§a[PV §l§a+"+ roll + "§l§a]");
        meta.setLore(Arrays.asList("§6§l[RANG ✪✪]", "PV", String.valueOf(roll), "§0Enchanting"));
    }
}
