package me.ortaria.metier.ressource.arcaniste.tier2;

import me.ortaria.utils.Dice;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class Tier2RechargeAstrale extends ItemStack {

    Dice dice;

    public Tier2RechargeAstrale(Dice dice){
        super(Material.BOOK, 1);
        this.dice = dice;
        ItemMeta meta = this.getItemMeta();
        int roll = dice.nombreDrop(5, 10);
        meta.setDisplayName("§l§5[Recharge Astale §l§5+"+ roll + "§l§l§5]");
        meta.setLore(Arrays.asList("§6§l[RANG ✪✪]", "RechargeAstrale", String.valueOf(roll), "§0Enchanting"));
    }

}
