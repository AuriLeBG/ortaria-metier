package me.ortaria.metier.ressource.bucheron;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class BoisDesDieux extends ItemStack {
    public BoisDesDieux(int amount){
        super(Material.GLOWSTONE_DUST, amount);
        ItemMeta meta = this.getItemMeta();
        meta.addEnchant(Enchantment.DURABILITY, 100, true);
        meta.setLore(Arrays.asList("§3[§5Ψ §cLe bois des dieux ! Il permet de faire tout et n'importe quoi!§5Ψ§3]", "§o§0[Vous devriez l'apporter à votre roi]"));
        meta.setDisplayName("§d✪§5Bois des dieux§d✪");
        this.setItemMeta(meta);
    }
}
