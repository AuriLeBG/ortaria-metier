package me.ortaria.metier.ressource.bucheron;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class BranchageLégendaire extends ItemStack {

    public BranchageLégendaire(int amount){
        super(Material.DEAD_BUSH, amount);
        ItemMeta meta = this.getItemMeta();
        meta.addEnchant(Enchantment.DURABILITY, 10, true);
        meta.setLore(Arrays.asList("§c[Le branchage le plus que rare ! Gardez le precieusement !]"));
        meta.setDisplayName("§6[§a۞§eBranchage legendaire§a۞§6]");
        this.setItemMeta(meta);
    }
}
