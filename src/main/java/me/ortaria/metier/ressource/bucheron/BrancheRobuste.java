package me.ortaria.metier.ressource.bucheron;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class BrancheRobuste extends ItemStack {
    public BrancheRobuste(int amount){
        super(Material.DEAD_BUSH, amount);
        ItemMeta meta = this.getItemMeta();
        meta.addEnchant(Enchantment.DURABILITY, 10, true);
        meta.setLore(Arrays.asList("§a[Une branche robuste !]"));
        meta.setDisplayName("§6[§eBranche robuste§6]");
        this.setItemMeta(meta);
    }
}
