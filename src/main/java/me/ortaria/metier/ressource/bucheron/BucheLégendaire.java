package me.ortaria.metier.ressource.bucheron;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class BucheLégendaire extends ItemStack {
    public BucheLégendaire(int amount){
        super(Material.OAK_LOG, amount);
        ItemMeta meta = this.getItemMeta();
        meta.addEnchant(Enchantment.DURABILITY, 10, true);
        meta.setLore(Arrays.asList("§c[La buche la plus que rare ! Gardez le precieusement !]"));
        meta.setDisplayName("§6[§a۞§eBuche legendaire§a۞§6]");
        this.setItemMeta(meta);
    }
}
