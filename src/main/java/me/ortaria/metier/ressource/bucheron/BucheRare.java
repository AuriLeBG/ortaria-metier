package me.ortaria.metier.ressource.bucheron;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class BucheRare extends ItemStack {
    public BucheRare(int amount){
        super(Material.OAK_LOG, amount);
        ItemMeta meta = this.getItemMeta();
        meta.addEnchant(Enchantment.DURABILITY, 10, true);
        meta.setLore(Arrays.asList("§a[Une bûche rare !]"));
        meta.setDisplayName("§6[§Bûche rare§6]");
        this.setItemMeta(meta);
    }
}
