package me.ortaria.metier.ressource.bucheron;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class BucheRobuste extends ItemStack {
    public BucheRobuste(int amount){
        super(Material.OAK_LOG, amount);
        ItemMeta meta = this.getItemMeta();
        meta.addEnchant(Enchantment.DURABILITY, 10, true);
        meta.setLore(Arrays.asList("§a[Une bûche robuste !]"));
        meta.setDisplayName("§6[§Bûche robuste§6]");
        this.setItemMeta(meta);
    }

}
