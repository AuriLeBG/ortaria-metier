package me.ortaria.metier.ressource.forgeron.tier1;

import me.ortaria.metier.ressource.mineur.Etain;
import me.ortaria.utils.GestionItem;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class JambièresRenforcées extends ItemStack{
    public JambièresRenforcées(){
        super(Material.DIAMOND_LEGGINGS, 1);
        ItemMeta customMeta = this.getItemMeta();
        customMeta.addEnchant(Enchantment.DURABILITY, 20, true);
        customMeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 8, true);
        customMeta.setLore(Arrays.asList( "§eDes jambières en diamant renforcées !", "Un forgeron compétent pourrais le renforcé !"));
        customMeta.setDisplayName("§3[Diamond leggings renforcés]");
        this.setItemMeta(customMeta);
    }

    static public void craft(Player player){
        if(player.getInventory().containsAtLeast(new Etain(0), 30) && player.getInventory().contains(Material.LAPIS_LAZULI, 200) && player.getInventory().contains(Material.DIAMOND, 50)){
            GestionItem.retirerItem(player, new Etain(1), 30);
            GestionItem.retirerItem(player, new ItemStack(Material.DIAMOND, 1), 50);
            GestionItem.retirerItem(player, new ItemStack(Material.LAPIS_LAZULI, 1), 200);
            player.getInventory().addItem(new JambièresRenforcées());
            player.updateInventory();
        }
        player.sendMessage("Vous n'avez pas les ressources necessaire");
    }

    public static ItemStack displayItem(){
        ItemStack item = new JambièresRenforcées();
        GestionItem.modifyLore(item, "§6[30 Etains]");
        GestionItem.modifyLore(item, "§6[50 diamonds]");
        GestionItem.modifyLore(item, "§6[200 lapis]");
        return item;
    }
}
