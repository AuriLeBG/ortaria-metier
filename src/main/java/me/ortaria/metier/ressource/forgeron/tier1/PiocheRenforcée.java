package me.ortaria.metier.ressource.forgeron.tier1;

import me.ortaria.metier.ressource.mineur.Etain;
import me.ortaria.utils.GestionItem;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class PiocheRenforcée extends ItemStack {
    public PiocheRenforcée(){
        super(Material.DIAMOND_PICKAXE, 1);
        ItemMeta customMeta = this.getItemMeta();
        customMeta.addEnchant(Enchantment.DURABILITY, 5, true);
        customMeta.addEnchant(Enchantment.DIG_SPEED, 3, true);
        customMeta.setLore(Arrays.asList( "§eUne pioche en diamant renforcée !", "Un forgeron compétent pourrais la renforcée !"));
        customMeta.setDisplayName("§3[Diamond pickaxe renforcé]");
        this.setItemMeta(customMeta);
    }

    static public void craft(Player player){
        if(player.getInventory().containsAtLeast(new Etain(0), 40) && player.getInventory().contains(Material.LAPIS_LAZULI, 200) && player.getInventory().contains(Material.DIAMOND, 50)){
            GestionItem.retirerItem(player, new Etain(1), 40);
            GestionItem.retirerItem(player, new ItemStack(Material.DIAMOND, 1), 50);
            GestionItem.retirerItem(player, new ItemStack(Material.LAPIS_LAZULI, 1), 200);
            player.getInventory().addItem(new PiocheRenforcée());
            player.updateInventory();
        }
        player.sendMessage("Vous n'avez pas les ressources necessaire");
    }

    public static ItemStack displayItem(){
        ItemStack item = new PiocheRenforcée();
        GestionItem.modifyLore(item, "§6[40 Etains]");
        GestionItem.modifyLore(item, "§6[50 diamonds]");
        GestionItem.modifyLore(item, "§6[200 lapis]");
        return item;
    }
}
