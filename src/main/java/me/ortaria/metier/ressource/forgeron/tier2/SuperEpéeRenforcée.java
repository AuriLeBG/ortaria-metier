package me.ortaria.metier.ressource.forgeron.tier2;

import me.ortaria.metier.ressource.forgeron.tier1.EpéeRenforcée;
import me.ortaria.metier.ressource.mineur.Saphir;
import me.ortaria.utils.GestionItem;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class SuperEpéeRenforcée extends ItemStack {
    public SuperEpéeRenforcée(){
        super(Material.DIAMOND_SWORD, 1);
        ItemMeta customMeta = this.getItemMeta();
        customMeta.addEnchant(Enchantment.DURABILITY, 25, true);
        customMeta.addEnchant(Enchantment.DAMAGE_ALL, 6, true);
        customMeta.addEnchant(Enchantment.DAMAGE_UNDEAD, 1, true);
        customMeta.setLore(Arrays.asList( "§eUne super épée en diamant renforcée !", "Un forgeron compétent pourrais la renforcée !"));
        customMeta.setDisplayName("§3[Super Diamond sword renforcé]");
        this.setItemMeta(customMeta);
    }

    static public void craft(Player player){
        if(player.getInventory().containsAtLeast(new Saphir(0), 5) && player.getInventory().containsAtLeast(new EpéeRenforcée(), 1) && player.getInventory().contains(Material.LAPIS_LAZULI, 200) && player.getInventory().contains(Material.DIAMOND, 50)){
            GestionItem.retirerItem(player, new Saphir(1), 5);
            GestionItem.retirerItem(player, new ItemStack(Material.DIAMOND, 1), 50);
            GestionItem.retirerItem(player, new ItemStack(Material.LAPIS_LAZULI, 1), 200);
            GestionItem.retirerItem(player, new EpéeRenforcée(), 1);
            player.getInventory().addItem(new SuperEpéeRenforcée());
            player.updateInventory();
        }
        player.sendMessage("Vous n'avez pas les ressources necessaire");
    }
    public static ItemStack displayItem(){
        ItemStack item = new SuperEpéeRenforcée();
        GestionItem.modifyLore(item, "§6[5 Saphir]");
        GestionItem.modifyLore(item, "§6[50 diamonds]");
        GestionItem.modifyLore(item, "§6[200 lapis]");
        GestionItem.modifyLore(item, "§6[Diamond sword renforcé]");
        return item;
    }
}
