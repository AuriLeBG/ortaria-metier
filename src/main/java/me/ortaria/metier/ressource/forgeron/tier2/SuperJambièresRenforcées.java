package me.ortaria.metier.ressource.forgeron.tier2;

import me.ortaria.metier.ressource.forgeron.tier1.JambièresRenforcées;
import me.ortaria.metier.ressource.mineur.Saphir;
import me.ortaria.utils.GestionItem;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class SuperJambièresRenforcées extends ItemStack {
    public SuperJambièresRenforcées(){
        super(Material.DIAMOND_LEGGINGS, 1);
        ItemMeta customMeta = this.getItemMeta();
        customMeta.addEnchant(Enchantment.DURABILITY, 25, true);
        customMeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 9, true);
        customMeta.setLore(Arrays.asList( "§eDes super jambières en diamant renforcées !", "Un forgeron compétent pourrais le renforcé !"));
        customMeta.setDisplayName("§3[Super Diamond leggings renforcés]");
        this.setItemMeta(customMeta);
    }

    static public void craft(Player player){
        if(player.getInventory().containsAtLeast(new Saphir(0), 5) && player.getInventory().containsAtLeast(new JambièresRenforcées(), 1) && player.getInventory().contains(Material.LAPIS_LAZULI, 200) && player.getInventory().contains(Material.DIAMOND, 50)){
            GestionItem.retirerItem(player, new Saphir(1), 5);
            GestionItem.retirerItem(player, new ItemStack(Material.DIAMOND, 1), 50);
            GestionItem.retirerItem(player, new ItemStack(Material.LAPIS_LAZULI, 1), 200);
            GestionItem.retirerItem(player, new JambièresRenforcées(), 1);
            player.getInventory().addItem(new SuperJambièresRenforcées());
            player.updateInventory();
        }
        player.sendMessage("Vous n'avez pas les ressources necessaire");
    }

    public static ItemStack displayItem(){
        ItemStack item = new SuperJambièresRenforcées();
        GestionItem.modifyLore(item, "§6[5 Saphir]");
        GestionItem.modifyLore(item, "§6[50 diamonds]");
        GestionItem.modifyLore(item, "§6[200 lapis]");
        GestionItem.modifyLore(item, "§6[Diamond leggings renforcés]");
        return item;
    }
}
