package me.ortaria.metier.ressource.forgeron.tier2;

import me.ortaria.metier.ressource.forgeron.tier1.PiocheRenforcée;
import me.ortaria.metier.ressource.mineur.Saphir;
import me.ortaria.utils.GestionItem;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class SuperPiocheRenforcée extends ItemStack {
    public SuperPiocheRenforcée(){
        super(Material.DIAMOND_PICKAXE, 1);
        ItemMeta customMeta = this.getItemMeta();
        customMeta.addEnchant(Enchantment.DURABILITY, 6, true);
        customMeta.addEnchant(Enchantment.DIG_SPEED, 4, true);
        customMeta.addEnchant(Enchantment.LOOT_BONUS_BLOCKS, 1, true);
        customMeta.setLore(Arrays.asList( "§eUne super pioche en diamant renforcée !", "Un forgeron compétent pourrais la renforcée !"));
        customMeta.setDisplayName("§3[Diamond pickaxe renforcé]");
        this.setItemMeta(customMeta);
    }

    static public void craft(Player player){
        if(player.getInventory().containsAtLeast(new Saphir(0), 5) && player.getInventory().containsAtLeast(new SuperPiocheRenforcée(), 1) && player.getInventory().contains(Material.LAPIS_LAZULI, 200) && player.getInventory().contains(Material.DIAMOND, 50)){
            GestionItem.retirerItem(player, new Saphir(5), 30);
            GestionItem.retirerItem(player, new ItemStack(Material.DIAMOND, 1), 50);
            GestionItem.retirerItem(player, new ItemStack(Material.LAPIS_LAZULI, 1), 200);
            GestionItem.retirerItem(player, new PiocheRenforcée(), 1);
            player.getInventory().addItem(new SuperPiocheRenforcée());
            player.updateInventory();
        }
        player.sendMessage("Vous n'avez pas les ressources necessaire");
    }

    public static ItemStack displayItem(){
        ItemStack item = new SuperPiocheRenforcée();
        GestionItem.modifyLore(item, "§6[5 Saphir]");
        GestionItem.modifyLore(item, "§6[50 diamonds]");
        GestionItem.modifyLore(item, "§6[200 lapis]");
        GestionItem.modifyLore(item, "§6[Diamond pickaxe renforcé]");
        return item;
    }
}
