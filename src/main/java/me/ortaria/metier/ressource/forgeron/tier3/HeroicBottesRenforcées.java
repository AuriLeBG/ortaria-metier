package me.ortaria.metier.ressource.forgeron.tier3;

import me.ortaria.metier.ressource.forgeron.tier1.BottesRenforcées;
import me.ortaria.metier.ressource.forgeron.tier2.SuperBottesRenforcées;
import me.ortaria.metier.ressource.forgeron.tier2.SuperCasqueRenforcé;
import me.ortaria.metier.ressource.forgeron.tier2.SuperPlastronRenforcé;
import me.ortaria.metier.ressource.mineur.Mithril;
import me.ortaria.utils.GestionItem;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class HeroicBottesRenforcées extends ItemStack {
    public HeroicBottesRenforcées(){
        super(Material.DIAMOND_BOOTS, 1);
        ItemMeta customMeta = this.getItemMeta();
        customMeta.addEnchant(Enchantment.DURABILITY, 30, true);
        customMeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 10, true);
        customMeta.setLore(Arrays.asList( "§eDes bottes en diamant renforcées digne d'un héro !", "Un forgeron compétent pourrais le renforcé !"));
        customMeta.setDisplayName("§3[Heroic Diamond boots renforcés]");
        this.setItemMeta(customMeta);
    }

    static public void craft(Player player){
        if(player.getInventory().containsAtLeast(new Mithril(0), 2) && player.getInventory().containsAtLeast(new SuperBottesRenforcées(), 1) && player.getInventory().contains(Material.LAPIS_LAZULI, 250) && player.getInventory().contains(Material.DIAMOND, 50)){
            GestionItem.retirerItem(player, new Mithril(1), 2);
            GestionItem.retirerItem(player, new ItemStack(Material.DIAMOND, 1), 50);
            GestionItem.retirerItem(player, new ItemStack(Material.LAPIS_LAZULI, 1), 250);
            GestionItem.retirerItem(player, new BottesRenforcées(), 1);
            player.getInventory().addItem(new HeroicBottesRenforcées());
            player.updateInventory();
        }
        player.sendMessage("Vous n'avez pas les ressources necessaire");
    }

    public static ItemStack displayItem(){
        ItemStack item = new HeroicBottesRenforcées();
        GestionItem.modifyLore(item, "§6[2 Mithril]");
        GestionItem.modifyLore(item, "§6[50 diamonds]");
        GestionItem.modifyLore(item, "§6[250 lapis]");
        GestionItem.modifyLore(item, "§6[Super Diamond boots renforcés]");
        return item;
    }
}
