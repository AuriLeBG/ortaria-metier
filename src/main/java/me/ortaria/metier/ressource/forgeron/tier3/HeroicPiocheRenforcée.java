package me.ortaria.metier.ressource.forgeron.tier3;

import me.ortaria.metier.ressource.forgeron.tier1.PiocheRenforcée;
import me.ortaria.metier.ressource.forgeron.tier2.SuperPiocheRenforcée;
import me.ortaria.metier.ressource.forgeron.tier2.SuperPlastronRenforcé;
import me.ortaria.metier.ressource.mineur.Mithril;
import me.ortaria.utils.GestionItem;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class HeroicPiocheRenforcée extends ItemStack {
    public HeroicPiocheRenforcée(){
        super(Material.DIAMOND_PICKAXE, 1);
        ItemMeta customMeta = this.getItemMeta();
        customMeta.addEnchant(Enchantment.DURABILITY, 8, true);
        customMeta.addEnchant(Enchantment.DIG_SPEED, 5, true);
        customMeta.addEnchant(Enchantment.LOOT_BONUS_BLOCKS, 1, true);
        customMeta.setLore(Arrays.asList( "§eUne pioche en diamant renforcée digne d'un héro !", "Un forgeron compétent pourrais la renforcée !"));
        customMeta.setDisplayName("§3[Heroic Diamond pickaxe renforcée]");
        this.setItemMeta(customMeta);
    }

    static public void craft(Player player){
        if(player.getInventory().containsAtLeast(new Mithril(0), 2) && player.getInventory().containsAtLeast(new me.ortaria.metier.ressource.forgeron.tier2.SuperPiocheRenforcée(), 1) && player.getInventory().contains(Material.LAPIS_LAZULI, 200) && player.getInventory().contains(Material.DIAMOND, 50)){
            GestionItem.retirerItem(player, new Mithril(5), 2);
            GestionItem.retirerItem(player, new ItemStack(Material.DIAMOND, 1), 50);
            GestionItem.retirerItem(player, new ItemStack(Material.LAPIS_LAZULI, 1), 200);
            GestionItem.retirerItem(player, new SuperPiocheRenforcée(), 1);
            player.getInventory().addItem(new HeroicPiocheRenforcée());
            player.updateInventory();
        }
        player.sendMessage("Vous n'avez pas les ressources necessaire");
    }

    public static ItemStack displayItem(){
        ItemStack item = new HeroicPiocheRenforcée();
        GestionItem.modifyLore(item, "§6[2 Mithril]");
        GestionItem.modifyLore(item, "§6[50 diamonds]");
        GestionItem.modifyLore(item, "§6[250 lapis]");
        GestionItem.modifyLore(item, "§6[Super Diamond pickaxe renforcé]");
        return item;
    }
}
