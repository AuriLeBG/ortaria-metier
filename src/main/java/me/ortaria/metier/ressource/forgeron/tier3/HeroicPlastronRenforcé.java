package me.ortaria.metier.ressource.forgeron.tier3;

import me.ortaria.metier.ressource.forgeron.tier2.SuperJambièresRenforcées;
import me.ortaria.metier.ressource.forgeron.tier2.SuperPlastronRenforcé;
import me.ortaria.metier.ressource.mineur.Mithril;
import me.ortaria.utils.GestionItem;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class HeroicPlastronRenforcé extends ItemStack {
    public HeroicPlastronRenforcé(){
        super(Material.DIAMOND_CHESTPLATE, 1);
        ItemMeta customMeta = this.getItemMeta();
        customMeta.addEnchant(Enchantment.DURABILITY, 30, true);
        customMeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 10, true);
        customMeta.setLore(Arrays.asList( "§eUn plastron en diamant renforcé digne d'un héro !", "Un forgeron compétent pourrais le renforcé !"));
        customMeta.setDisplayName("§3[Heroic Diamond chestplate renforcé]");
        this.setItemMeta(customMeta);
    }

    static public void craft(Player player){
        if(player.getInventory().containsAtLeast(new Mithril(0), 2) && player.getInventory().containsAtLeast(new SuperPlastronRenforcé(), 1) && player.getInventory().contains(Material.LAPIS_LAZULI, 250) && player.getInventory().contains(Material.DIAMOND, 50)){
            GestionItem.retirerItem(player, new Mithril(1), 2);
            GestionItem.retirerItem(player, new ItemStack(Material.DIAMOND, 1), 50);
            GestionItem.retirerItem(player, new ItemStack(Material.LAPIS_LAZULI, 1), 2);
            GestionItem.retirerItem(player, new SuperPlastronRenforcé(), 1);
            player.getInventory().addItem(new HeroicPlastronRenforcé());
            player.updateInventory();
        }
        player.sendMessage("Vous n'avez pas les ressources necessaire");
    }

    public static ItemStack displayItem(){
        ItemStack item = new HeroicPlastronRenforcé();
        GestionItem.modifyLore(item, "§6[2 Mithril]");
        GestionItem.modifyLore(item, "§6[50 diamonds]");
        GestionItem.modifyLore(item, "§6[250 lapis]");
        GestionItem.modifyLore(item, "§6[Super Diamond chestplate renforcé]");
        return item;
    }
}
