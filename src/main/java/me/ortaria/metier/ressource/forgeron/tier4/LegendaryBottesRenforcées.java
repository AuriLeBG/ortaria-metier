package me.ortaria.metier.ressource.forgeron.tier4;

import me.ortaria.metier.ressource.forgeron.tier3.HeroicBottesRenforcées;
import me.ortaria.metier.ressource.forgeron.tier3.HeroicCasqueRenforcé;
import me.ortaria.metier.ressource.forgeron.tier3.HeroicPlastronRenforcé;
import me.ortaria.metier.ressource.mineur.Adamantium;
import me.ortaria.utils.GestionItem;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class LegendaryBottesRenforcées extends ItemStack {
    public LegendaryBottesRenforcées(){
        super(Material.DIAMOND_BOOTS, 1);
        ItemMeta customMeta = this.getItemMeta();
        customMeta.addEnchant(Enchantment.DURABILITY, 40, true);
        customMeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 11, true);
        customMeta.setLore(Arrays.asList("A MODIFIE APT", "§eDes bottes en diamant renforcées Légendaire!", "La plus belle oeuvre du forgeron de la ville !"));
        customMeta.setDisplayName("§3[Legendary Diamond boots renforcés]");
        this.setItemMeta(customMeta);
    }

    static public void craft(Player player){
        if(player.getInventory().containsAtLeast(new Adamantium(0), 1) && player.getInventory().containsAtLeast(new HeroicBottesRenforcées(), 1) && player.getInventory().contains(Material.LAPIS_LAZULI, 250) && player.getInventory().contains(Material.DIAMOND, 50)){
            GestionItem.retirerItem(player, new Adamantium(1), 1);
            GestionItem.retirerItem(player, new ItemStack(Material.DIAMOND, 1), 50);
            GestionItem.retirerItem(player, new ItemStack(Material.LAPIS_LAZULI, 1), 250);
            GestionItem.retirerItem(player, new HeroicBottesRenforcées(), 1);
            player.getInventory().addItem(new LegendaryBottesRenforcées());
            player.updateInventory();
        }
        player.sendMessage("Vous n'avez pas les ressources necessaire");
    }

    public static ItemStack displayItem(){
        ItemStack item = new LegendaryBottesRenforcées();
        GestionItem.modifyLore(item, "§6[1 Adamantium]");
        GestionItem.modifyLore(item, "§6[50 diamonds]");
        GestionItem.modifyLore(item, "§6[250 lapis]");
        GestionItem.modifyLore(item, "§6[Heroic Diamond boots renforcés]");
        return item;
    }
}
