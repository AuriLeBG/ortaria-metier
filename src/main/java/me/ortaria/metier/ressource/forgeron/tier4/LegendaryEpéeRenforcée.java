package me.ortaria.metier.ressource.forgeron.tier4;

import me.ortaria.metier.ressource.forgeron.tier3.HeroicEpéeRenforcée;
import me.ortaria.metier.ressource.mineur.Adamantium;
import me.ortaria.utils.GestionItem;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class LegendaryEpéeRenforcée extends ItemStack {
    public LegendaryEpéeRenforcée(){
        super(Material.DIAMOND_SWORD, 1);
        ItemMeta customMeta = this.getItemMeta();
        customMeta.addEnchant(Enchantment.DURABILITY, 40, true);
        customMeta.addEnchant(Enchantment.DAMAGE_ALL, 10, true);
        customMeta.addEnchant(Enchantment.DAMAGE_UNDEAD, 3, true);
        customMeta.addEnchant(Enchantment.FIRE_ASPECT, 2, true);
        customMeta.addEnchant(Enchantment.LOOT_BONUS_MOBS, 3, true);
        customMeta.setLore(Arrays.asList("§eUne épée en diamant renforcée Légendaire!", "La plus belle oeuvre du fondeur !"));
        customMeta.setDisplayName("§3[Legendary Diamond sword renforcé]");
        this.setItemMeta(customMeta);
    }

    static public void craft(Player player){
        if(player.getInventory().containsAtLeast(new Adamantium(0), 1) && player.getInventory().containsAtLeast(new HeroicEpéeRenforcée(), 1) && player.getInventory().contains(Material.LAPIS_LAZULI, 250) && player.getInventory().contains(Material.DIAMOND, 50)){
            GestionItem.retirerItem(player, new Adamantium(1), 1);
            GestionItem.retirerItem(player, new ItemStack(Material.DIAMOND, 1), 50);
            GestionItem.retirerItem(player, new ItemStack(Material.LAPIS_LAZULI, 1), 250);
            GestionItem.retirerItem(player, new HeroicEpéeRenforcée(), 1);
            player.getInventory().addItem(new LegendaryEpéeRenforcée());
            player.updateInventory();
        }
        player.sendMessage("Vous n'avez pas les ressources necessaire");
    }

    public static ItemStack displayItem(){
        ItemStack item = new LegendaryEpéeRenforcée();
        GestionItem.modifyLore(item, "§6[1 Adamantium]");
        GestionItem.modifyLore(item, "§6[50 diamonds]");
        GestionItem.modifyLore(item, "§6[250 lapis]");
        GestionItem.modifyLore(item, "§6[Heroic Diamond sword renforcé]");
        return item;
    }
}
