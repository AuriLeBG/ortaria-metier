package me.ortaria.metier.ressource.mineur;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class Adamantium extends ItemStack {
    public Adamantium(int amount){
        super(Material.EMERALD, amount);
        ItemMeta customMeta = this.getItemMeta();
        customMeta.addEnchant(Enchantment.DURABILITY, 10, true);
        customMeta.setLore(Arrays.asList("§c[Un minerais plus que rare ! Gardez le precieusement !]"));
        customMeta.setDisplayName("§2[§5۞§eADAMANTIUM§5۞§2]");
        this.setItemMeta(customMeta);
    }
}
