package me.ortaria.metier.ressource.mineur;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class Cuivre extends ItemStack {
    public Cuivre(int amount){
        super(Material.GRANITE, amount);
        ItemMeta customMeta = this.getItemMeta();
        customMeta.addEnchant(Enchantment.DURABILITY, 10, true);
        customMeta.setLore(Arrays.asList("§e[Un lingot de cuivre brut!]"));
        customMeta.setDisplayName("§c[§6Lingot de cuivre brut§c]");
        this.setItemMeta(customMeta);
    }
}
