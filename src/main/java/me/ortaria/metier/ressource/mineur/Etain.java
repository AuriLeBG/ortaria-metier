package me.ortaria.metier.ressource.mineur;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class Etain extends ItemStack {

    public Etain(int amount){
        super(Material.IRON_INGOT, amount);
        ItemMeta customMeta = this.getItemMeta();
        customMeta.addEnchant(Enchantment.DURABILITY, 10, true);
        customMeta.setLore(Arrays.asList("§e[Un lingot d'Etain tout ce qu'il y a de plus classique]"));
        customMeta.setDisplayName("§6[§7Lingot d'Etain§6]");
        this.setItemMeta(customMeta);
    }


}
