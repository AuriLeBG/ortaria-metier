package me.ortaria.metier.ressource.mineur;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class Mithril extends ItemStack {
    public Mithril(int amount){
        super(Material.PRISMARINE_SHARD, amount);
        ItemMeta customMeta = this.getItemMeta();
        customMeta.addEnchant(Enchantment.DURABILITY, 10, true);
        customMeta.setLore(Arrays.asList("§c[Un minerais plus que rare ! Gardez le precieusement !]"));
        customMeta.setDisplayName("§6[§a۞§3MITHRIL§a۞§6]");
        this.setItemMeta(customMeta);
    }
}
