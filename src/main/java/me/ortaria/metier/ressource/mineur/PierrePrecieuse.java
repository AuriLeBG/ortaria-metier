package me.ortaria.metier.ressource.mineur;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class PierrePrecieuse extends ItemStack {
    public PierrePrecieuse(int amount){
        super(Material.GLOWSTONE_DUST, amount);
        ItemMeta customMeta = this.getItemMeta();
        customMeta.addEnchant(Enchantment.DURABILITY, 100, true);
        customMeta.setLore(Arrays.asList("§3[§5Ψ §cUne pierre qui vaut des millions ! Elle permet de faire tout et n'importe quoi!§5Ψ§3]", "§o§0[Vous devriez l'apporter à votre roi]"));
        customMeta.setDisplayName("§d✪§5Pierre Precieuse§d✪");
        this.setItemMeta(customMeta);
    }
}
