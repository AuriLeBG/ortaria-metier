package me.ortaria.metier.ressource.mineur;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class Rubis extends ItemStack {
    public Rubis(int amount){
        super(Material.REDSTONE_BLOCK, amount);
        ItemMeta customMeta = this.getItemMeta();
        customMeta.addEnchant(Enchantment.DURABILITY, 10, true);
        customMeta.setLore(Arrays.asList("§e[Un rubis! C'est une pierre precieuse !]"));
        customMeta.setDisplayName("§6[§cRubis§6]");
        this.setItemMeta(customMeta);
    }
}
