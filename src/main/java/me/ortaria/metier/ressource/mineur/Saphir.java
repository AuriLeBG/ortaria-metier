package me.ortaria.metier.ressource.mineur;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class Saphir extends ItemStack {
    public Saphir(int amount){
        super(Material.LAPIS_BLOCK, amount);
        ItemMeta customMeta = this.getItemMeta();
        customMeta.addEnchant(Enchantment.DURABILITY, 10, true);
        customMeta.setLore(Arrays.asList("§e[Un saphir ! C'est une pierre precieuse !]", "§2[Un mage pourrais en avoir besoin...]"));
        customMeta.setDisplayName("§6[§bSaphir§6]");
        this.setItemMeta(customMeta);
    }
}
