package me.ortaria.metier.ressource.pelleteur;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class FossileEmbourbé extends ItemStack {
    public FossileEmbourbé(){
        super(Material.BONE);
        ItemMeta customMeta = this.getItemMeta();
        customMeta.setDisplayName("§2[§aFossile Embourbé§2]");
        customMeta.addEnchant(Enchantment.DURABILITY, 10, true);
        customMeta.setLore(Arrays.asList("§e[Un fossile emplit de boue !]", "§e☆☆"));
        this.setItemMeta(customMeta);
    }
}
