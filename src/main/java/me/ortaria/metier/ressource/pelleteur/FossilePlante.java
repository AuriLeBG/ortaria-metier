package me.ortaria.metier.ressource.pelleteur;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class FossilePlante extends ItemStack {
    public FossilePlante(){
        super(Material.BONE);
        ItemMeta customMeta = this.getItemMeta();
        customMeta.setDisplayName("§2[§e۞§aFossile plante§e۞§2]");
        customMeta.addEnchant(Enchantment.DURABILITY, 10, true);
        customMeta.setLore(Arrays.asList("§d[§bUn fossile Complex !§d]", "§d✿"));
        this.setItemMeta(customMeta);
    }
}
