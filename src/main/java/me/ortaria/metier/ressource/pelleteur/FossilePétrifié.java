package me.ortaria.metier.ressource.pelleteur;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class FossilePétrifié extends ItemStack {
    public FossilePétrifié(){
        super(Material.BONE);
        ItemMeta customMeta = this.getItemMeta();
        customMeta.setDisplayName("§2[§aFossile Pétrifié§2]");
        customMeta.addEnchant(Enchantment.DURABILITY, 10, true);
        customMeta.setLore(Arrays.asList("§e[Un fossile ou le matériau organique du fossile a été remplacé par des minéraux !]", "§e☆☆☆☆"));
        this.setItemMeta(customMeta);
    }
}
