package me.ortaria.utils;

import java.util.Random;

public class Dice {
    Random random;

    public Dice(){
        this.random = new Random();
    }

    public int nombreDrop(int minRoll, int maxRoll){
        int result = this.random.nextInt(maxRoll + 1 - minRoll);
        return result + minRoll;
    }

    public Boolean tauxDrop(double taux){
        Double result = this.random.nextDouble() * 100;
        return result < taux;
    }
}
