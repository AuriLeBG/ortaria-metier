package me.ortaria.utils;

import net.minecraft.world.item.ItemMapEmpty;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;

public class GestionItem {

    public static void retirerItem(Player player, ItemStack item, int amount){
        int nbItem = 0;
        for(int i = 0; i < player.getInventory().getSize(); i++){
            if(item.isSimilar(player.getInventory().getItem(i))){
                nbItem += player.getInventory().getItem(i).getAmount();
            }
        }
        if(nbItem >= amount){
            for(int i = 0; i < player.getInventory().getSize(); i++){
                if(item.isSimilar(player.getInventory().getItem(i))){
                    player.getInventory().setItem(i, new ItemStack(Material.VOID_AIR, 0));
                }
            }
            item.setAmount(nbItem - amount);
            player.getInventory().addItem(item);
        }
    }

    public static void retirerWaterBottle(Player player, int amount){
        int nbItem = 0;
        ItemStack item = null;
        for(int i = 0; i < player.getInventory().getSize(); i++){
            if(player.getInventory().getItem(i) != null && player.getInventory().getItem(i).getTranslationKey().equalsIgnoreCase("item.minecraft.potion.effect.water")){
                nbItem += player.getInventory().getItem(i).getAmount();
                item = player.getInventory().getItem(i);
            }
        }
        if(nbItem >= amount){
            for(int i = 0; i < player.getInventory().getSize(); i++){
                if(item != null && item.isSimilar(player.getInventory().getItem(i))){
                    player.getInventory().setItem(i, new ItemStack(Material.VOID_AIR, 0));
                }
            }
            for(int i = 0; i < nbItem - amount; i ++){
                player.getInventory().addItem(item);
            }
        }
    }

    public static void retirerItemSlot(Player player, int amount){
        int slot = player.getInventory().getHeldItemSlot();
        player.getInventory().getItem(slot).setAmount(player.getInventory().getItem(slot).getAmount() - 1);
    }




    public static ItemStack getItem(Material material, String customName){
        ItemStack item = new ItemStack(material, 1);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(customName);
        item.setItemMeta(itemMeta);
        return item;
    }

    public static void modifyLore(ItemStack item, String lore){
        ItemMeta itemMeta = item.getItemMeta();
        List <String> lore1 =  Arrays.asList(lore);
        if (itemMeta.hasLore()) {
            lore1 = itemMeta.getLore();
            lore1.add(lore);
        }
        itemMeta.setLore(lore1);
        item.setItemMeta(itemMeta);
    }
}
