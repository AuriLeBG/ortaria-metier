package me.ortaria.utils;

import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class GestionMenu {
    static public void fillBorder54(Inventory inv, Material material){
        for(int i = 0; i <= 9; i ++){
            inv.setItem(i, new ItemStack(material, 1));
        }
        for(int i = 44; i <= 53; i++){
            inv.setItem(i, new ItemStack(material, 1));
        }
        inv.setItem(17, new ItemStack(material, 1));
        inv.setItem(18, new ItemStack(material, 1));
        inv.setItem(26, new ItemStack(material, 1));
        inv.setItem(27, new ItemStack(material, 1));
        inv.setItem(35, new ItemStack(material, 1));
        inv.setItem(36, new ItemStack(material, 1));
    }

    static public void fillBorder45(Inventory inv, Material material){
        for(int i = 0; i <= 9; i ++){
            inv.setItem(i, new ItemStack(material, 1));
        }
        for(int i = 35; i <= 44; i++){
            inv.setItem(i, new ItemStack(material, 1));
        }
        inv.setItem(17, new ItemStack(material, 1));
        inv.setItem(18, new ItemStack(material, 1));
        inv.setItem(26, new ItemStack(material, 1));
        inv.setItem(27, new ItemStack(material, 1));

    }

}
